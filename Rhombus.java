package com.HW_ShapeDrawings;

public class Rhombus {

    public static String rhombus(int rhombusSide) {
        for (int i = 1; i < rhombusSide; ++i) {
            for (int j = rhombusSide; j > i; --j)
                System.out.print(" ");
            for (int j = 1; j < 2 * i; ++j)
                System.out.print("*");
            System.out.println();
        }

        for (int i = rhombusSide; i >= 1; --i) {
            for (int j = rhombusSide; j > i; --j)
                System.out.print(" ");
            for (int j = 1; j < i * 2; ++j)
                System.out.print("*");
            System.out.println();
        }
        return "";
    }

    public static void main(String[] args) {
        System.out.println(rhombus(6));
    }
}
