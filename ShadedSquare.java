package com.HW_ShapeDrawings;

public class ShadedSquare {

    public static String shadedSquare(int sideLength) {
        for (int j = 0; j < sideLength; j++) {
            for (int i = 0; i < sideLength; i++) {
                System.out.print("* ");
            }
            System.out.println();
        }
        return "";
    }

    public static void main(String[] args) {
        System.out.println(shadedSquare(5));
    }

}
