package com.HW_ShapeDrawings;

public class Triangle7 {

    public static String triangle7(int triangleSide) {
        for (int i = triangleSide; i > 0; i--) {
            for (int j = triangleSide; j >= i; j--) {
                System.out.print("* ");
            }
            System.out.println();
        }

        for (int i = 0; i < triangleSide; i++) {
            for (int j = i; j < triangleSide - 1; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
        return "";
    }

    public static void main(String[] args) {
        System.out.println(triangle7(5));
    }
}
