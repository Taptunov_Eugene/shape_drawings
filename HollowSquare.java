package com.HW_ShapeDrawings;

public class HollowSquare {

    public static String hollowSquare(int sideLength) {
        for (int i = 0; i < sideLength; i++) {
            for (int j = 0; j < sideLength; j++) {
                if (i == 0 || i == sideLength - 1 ||
                        j == 0 || j == sideLength - 1)
                    System.out.print("* ");
                else
                    System.out.print("  ");
            }
            System.out.println();
        }
        return "";
    }

    public static void main(String[] args) {
        System.out.println(hollowSquare(7));
    }
}
