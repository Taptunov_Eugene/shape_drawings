package com.HW_ShapeDrawings;

public class Triangle6 {

    public static String triangle6(int triangleSide) {
        for (int y = triangleSide; y > 0; y--) {
            for (int space = triangleSide; space > y; space--)
                System.out.print(" ");
            for (int x = 1; x <= y; x++)
                System.out.print("*");
            System.out.println();
        }
        return "";
    }

    public static void main(String[] args) {
        System.out.println(triangle6(5));
    }
}
