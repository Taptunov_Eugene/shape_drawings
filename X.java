package com.HW_ShapeDrawings;

public class X {
    public static void main(String[] args) {

        System.out.println(drawCross(13));
    }

    public static String drawCross(int size) {
        StringBuilder stringBuilder = new StringBuilder();
        int gap = size;
        for (int j = 0; j < size; j++) {
            for (int i = 0; i < size; i++) {
                if (i == size - gap || i == gap - 1) {
                    stringBuilder.append("*");
                } else {
                    stringBuilder.append(" ");
                }
            }
            stringBuilder.append('\n');
            gap--;
        }
        return stringBuilder.toString();
    }
}
