package com.HW_ShapeDrawings;

public class Triangle2 {

    public static String triangle2(int triangleSide) {
        for (int i = triangleSide; i > 0; i--) {
            for (int j = triangleSide; j >= i; j--) {
                System.out.print("* ");
            }
            System.out.println();
        }
        return "";
    }

    public static void main(String[] args) {
        System.out.println(triangle2(5));
    }
}
