package com.HW_ShapeDrawings;

public class Triangle1 {

    public static String triangle1(int triangleSide) {
        for (int i = 0; i < triangleSide; i++) {
            for (int j = i; j < triangleSide; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
        return "";
    }

    public static void main(String[] args) {
        System.out.println(triangle1(5));
    }
}
