package com.HW_ShapeDrawings;

public class Triangle5 {

    public static String triangle5(int triangleSide) {
        for (int y = 1; y <= triangleSide; y++) {
            for (int space = triangleSide; space > y; space--)
                System.out.print(" ");
            for (int x = 1; x <= y; x++)
                System.out.print("*");
            System.out.println();
        }
        return "";
    }

    public static void main(String[] args) {
        System.out.println(triangle5(5));
    }
}
