package com.HW_ShapeDrawings;

public class Circle {

    public static String circle(int radius) {
        int x, y;
        for (int i = 0; i <= 2 * radius; i++) {
            for (int j = 0; j <= 2 * radius; j++) {
                x = i - radius;
                y = j - radius;
                if (x * x + y * y <= radius * radius + 1)
                    System.out.print(" * ");
                else
                    System.out.print("   ");
                System.out.print("   ");
            }
            System.out.println();
        }
        return "";
    }

    public static void main(String[] args) {
        System.out.println(circle(3));
    }
}
